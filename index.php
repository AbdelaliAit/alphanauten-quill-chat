<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content />
        <meta name="author" content />
        <title>Quill - Chat</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/aos/aos.css" />
        <link href="css/main.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="js/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
        <script src="js/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="layoutDefault">
            <div id="layoutDefault_content">
                <main>
                    <?php include_once("includes/header.php") ?>

                    <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
                        <div class="page-header-content ">
                            <div class="container">
                                <div class="row align-items-center">
                                    <div class="col-lg-12 d-none d-lg-block logo-container" data-aos="fade-up" >
                                     
                                        <div class="logo-box">

                                            <div class="logo-groupe">
                                            <img class="img-fluid" src="assets/img/logo/background.png" />
                                            <img class="img-fluid" src="assets/img/logo/richat.png"  />
                                            <img class="img-fluid" src="assets/img/logo/Bublle.png" />
                                             </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 align-items-center" data-aos="fade-up" data-aos-delay="200">
                                        <div class="header-box">
                                        <h1 class="page-header-title">Messaging to make your team better. Meet Quill.o</h1>
                                        <a class="header-button" href="javascript:void(0)">Try it free</a>
                                        <p class="header-description mb-5">Available for web, macOS, Windows, Linux, iOS, and Android.</p>
                                         </div>
                                   
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                       
                    </header>
                    <section class="bg-black py-10">
                        <div class="container">
                            <div class="quill-message-section" data-aos="fade-down">
                                <h1 class="quill-message-section-header"> Quill is messaging for people that focus.</h1>
                                <p  class="quill-message-section-description">
                                 We love messaging. It’s our favorite way of collaborating, but not if it’s overwhelming and disorganized. We believe there’s a better way.
                                </p>
                                <p  class="quill-message-section-sub-description">
                                 We grew exhausted having to skim thousands of messages every day to keep up, so we built a way to chat that’s even better than how we already communicate in person. A more deliberate way to chat. That’s what Quill is all about.
                                </p>
                             </div>

                             <div class="row">
                             <div class="col-md-12 quill-message-box">

                                 <div class="col-md-5 quill-message-box-text">

                                    <img src="assets/img/icons/section_messaging_chat.png">
                                    <h2>One conversation at a time. Messaging bliss.</h2>
                                    <p class="quill-message-box-desc">
                                        Threads keep your conversations discoverable and organized.
                                    </p>
                                    <p class="quill-message-box-sub-desc">
                                        Revive ideas by sending a new message to a previous conversation. With history just a scroll away, everyone starts off up to date.
                                    </p>
                                 </div>
                                 <div class="col-md-8 quill-message-box-video">
                                     </div>

                             </div>
                             </div>


                             <div class="row">
                                 
                                 <div class=" quill-message-box-detail">
                                    <img src="assets/img/icons/activity_feed.png" class="header-img">
                                    <h2>Activity Feed</h2> 
                                    <p>See your most important conversations first. Snooze, archive, or jump back in.</p>
                                    <span >Activity Feed always keeps your conversations within reach, so you can quickly catch up on what’s new.</span>
                                    <img class="video" src="assets/img/icons/iphone_framepng.png">   
                                </div>
                                 <div class=" quill-message-box-detail">
                                    <img src="assets/img/icons/activity_feed.png" class="header-img">
                                    <h2>Activity Feed</h2> 
                                    <p>See your most important conversations first. Snooze, archive, or jump back in.</p>
                                    <span >Activity Feed always keeps your conversations within reach, so you can quickly catch up on what’s new.</span>
                                    <img class="video" src="assets/img/icons/iphone_framepng.png">   
                                </div>
                             </div>


                        </div>
                        
                    </section>

                   
                    <section class="bg-light py-10 message-section-white">
                        <div class="container ">
                        <div class="quill-message-section " data-aos="fade-down">
                                <h1 class="quill-message-section-header"> Quill is messaging for people that focus.</h1>
                                <p  class="quill-message-section-description">
                                 We love messaging. It’s our favorite way of collaborating, but not if it’s overwhelming and disorganized. We believe there’s a better way.
                                </p>
                                <p  class="quill-message-section-sub-description">
                                 We grew exhausted having to skim thousands of messages every day to keep up, so we built a way to chat that’s even better than how we already communicate in person. A more deliberate way to chat. That’s what Quill is all about.
                                </p>
                             </div>

                             <div class="row">
                             <div class="col-md-12 quill-message-box">

                                 <div class="col-md-5 quill-message-box-text">

                                    <img src="assets/img/icons/section_messaging_chat.png">
                                    <h2>One conversation at a time. Messaging bliss.</h2>
                                    <p class="quill-message-box-desc">
                                        Threads keep your conversations discoverable and organized.
                                    </p>
                                    <p class="quill-message-box-sub-desc">
                                        Revive ideas by sending a new message to a previous conversation. With history just a scroll away, everyone starts off up to date.
                                    </p>
                                 </div>
                                 <div class="col-md-8 quill-message-box-video">
                                     </div>

                             </div>
                             </div>


                             <div class="row">
                                 
                                 <div class=" quill-message-box-detail">
                                     <h2>Retroactive Threads</h2> 
                                    <p>Retroactive Threads.</p>
                                    <span >Activity Feed always keeps your conversations within reach, so you can quickly catch up on what’s new.</span>
                                    <img class="video" src="assets/img/icons/graphic_replies.jpg">   
                                </div>
                                 <div class=" quill-message-box-detail">
                                    <h2>Replies</h2> 
                                    <p>Reply directly to a message..</p>
                                    <span >For those quick back-and-forths..</span>
                                    <img class="video" src="assets/img/icons/graphic_replies.jpg">   
                                </div>
                             </div>


                        </div>
                        </div>
                    </section>
                  
            

            <?php include_once("includes/footer.php") ?>
        </div>
        <script src="js/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="js/bootstrap/dist/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="js/aos/aos.js"></script>
        <script>
            AOS.init({
                disable: 'mobile',
                duration: 600,
                once: true,
            });
        </script>

      
    </body>

</html>
