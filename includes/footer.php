<footer>
       
        <!-- End Contact Container -->


        <div class="copyright">
     
        <div class="container">


                <div class="row">
                        <div class="col-md-12 quill-message-box mb60">
                            <div class="card-body p-5 ">
                            
                            <div class="text-center mb-5">
                            <img src="assets/img/logo.png" width="256" height="256" class="img-reponsive">
                                <h2 class="text-white">The best messaging experience. Now available for work.</h2>
                                <p class="lead">Quill is free for small groups and teams, and scales up to even the largest companies and enterprises.</p>
                                <a class="btn btn-teal font-weight-500 btn-white" href="#!">Try it free</a>
                                <br><br>
                                <a href="#" >Available for web, macOS, Windows, Linux, iOS, and Android.</a>
                            </div> 
                        </div>
                           
                            

                        </div>
                </div>

            
                
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3"> 
                            <img src="assets/img/chat.png"  class="img-responsive img-footer"/>
                       </div>
                        <div class="col-md-7">
                            <span class="footer-text"><b>We love messaging</b>. We’ve built, launched and scaled products at Apple, Square, Stripe, Snap, Wish, and OpenAI. If you care deeply about high quality software — we’d love to chat. <a href="#" class="footer-text-bleu">Jobs&nbsp;at&nbsp;Quill&nbsp;→</a> </span>
                        </div>
                    <div class="col-md-1"></div>
                    
                </div>
                <div class="row">

      
                        
                    <div class="col-md-8">
                        <div class="copyright-menu">
                            <ul>
                                <li>
                                    <a href="#">Overview</a>
                                </li>
                                <li>
                                    <a href="#">Pricing</a>
                                </li>
                               
                                <li>
                                    <a href="#">Updates</a>
                                </li>

                                <li>
                                    <a href="#">API Documentation</a>
                                </li>
                                <li>
                                    <a href="#">Newsletter</a>
                                </li>
                               
                            </ul>

                        </div>
                    </div>
                    <div class="col-md-4">
                    <ul class="navbar-nav footer-menu" >
                        <li  class="nav-item  float-left mr-10px" >
                            <a class="nav-link font-weight-500 " href="#">Download</a>
                        </li>
                        <li  class="nav-item float-left mrg-auto mr-10px">
                            <a class="btn font-weight-500 btn-blue-style btn-header text-white" href="#">Try it free</a>
                        </li>
                    </ul>
                    </div>
                    <!-- End Col -->
                   
                </div>

            </div>    
                <!-- End Row --> 
        </div>
        
        <div class="copyright">
            <div class="container brd-1">
                <div class="row">
                    <div class="col-md-10">
                        <div class="copyright-menu">
                            <ul>
                                <li>
                                    <a href="#">About</a>
                                </li>
                                <li>
                                    <a href="#">Jobs</a>
                                </li>
                                <li>
                                    <a href="#">Press</a>
                                </li>
                                <li>
                                    <a href="#">Security</a>
                                </li>

                                <li>
                                    <a href="#">Contact</a>
                                </li>
                                <li>
                                    <a href="#">Terms</a>
                                </li>
                                <li>
                                    <a href="#">Privacy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <i class="ml-2" data-feather="twitter"></i>
                    </div>
                    <!-- End Col -->
                   
                </div>

            </div>    
                <!-- End Row --> 
        </div>
        <!-- End Copyright -->
</footer>